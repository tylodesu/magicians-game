extends Node

# Class describes technology tree for country
class CountryTechTree:
	var _tree = {
		'pottery': [1, []], # tech need, dependencies
		'animal_husbandry': [1, []],
		'mining': [1, []],
		'water': [1, []],
		'faith': [1, []],
		'bow': [4, ['pottery']],
		'write': [6, ['pottery']],
		'expense': [9, ['write', 'animal_husbandry']],
		'wheel': [7, ['mining', 'animal_husbandry']],
		'copper': [5, ['mining']],
		'star_nav': [12, ['water', 'faith']],
		'engeneering': [16, ['write', 'expense']],
		'currency': [21, ['expense', 'copper']],
		'iron': [25, ['copper']],
		'mechanics': [29, ['bow', 'engeneering']],
		'math': [35, ['currency']],
		'horse': [29, ['wheel']],
		'shipbuilding': [25, ['water', 'star_nav', 'mechanics']],
		'print': [39, ['mechanics']],
		'military_tactics': [41, ['math', 'horse']],
		'bank': [42, ['math']],
		'blacksmith': [45, ['iron']],
		'astronomy': [29, ['shipbuilding']],
		'industries': [31, ['shipbuilding', 'print', 'blacksmith']],
		'knights': [49, ['military_tactics', 'blacksmith']],
		'economy': [50, ['banking']],
		'steam': [42, ['industries']],
		'gunpowder': [55, ['industries', 'knights']],
		'science': [55, ['economy']],
		'casting': [49, ['blacksmith']],
		'electricity': [49, ['steam']],
		'cars': [64, ['steam', 'science']],
		'aircraft': [57, ['industries', 'science']],
		'medicine': [62, ['science']],
		'steel': [58, ['casting']],
		'ballistics': [75, ['casting', 'gunpowder']],
		'circuits': [89, ['electricity']],
		'radio': [89, ['electricity']],
		'chemistry': [95, ['medicine']],
		'planes': [110, ['aircraft']],
		'rocket': [110, ['chemistry', 'ballistics']],
		'plastic': [115, ['steel', 'chemistry']],
		'computers': [130, ['circuits']],
		'synth': [145, ['plastic']],
		'atom': [175, ['chemistry']],
		'telecommunication': [195, ['computers']],
		'sattelite': [201, ['rocket']],
		'composite': [195, ['synth']],
		'robotics': [250, ['computers']],
		'nano': [275, ['composite']],
		'nuclear': [300, ['atom']]
	};
	
	var currentTechs = [];
	var researchTech : Array = null; # [int(tech left), String(name)]
	
	# Start research
	func initTech(name : String):
		if researchTech != null:
			return false;
		var deps = _tree[name];
		for i in deps[1]:
			if !(i in currentTechs):
				return false;
		researchTech = [deps[0], name];
		return true;
	
	# Update turns to end of research
	func researchTurns(sciLevel : int):
		if researchTech == null:
			return 0
		else:
			if sciLevel > researchTech[0]:
				currentTechs.append(researchTech[1]);
				researchTech = null;
				return 0;
			else:
				var turns = ceil(researchTech[0] / sciLevel);
				researchTech[0] -= sciLevel;
				return turns;
