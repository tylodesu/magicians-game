extends Node

# Class describe city
class City:
	var upgradeList = {
		'warehouse': [null, 85],
		'military_camp': [null, 89],
		'tavern': [null, 120],
		'temple': ['faith', 135],
		'walls': ['mining', 140],
		'school': ['expense', 145],
		'stone_roads': ['wheel', 150],
		'market': ['currency', 175],
		'workshop': ['iron', 189],
		'sewerage': ['mechanics', 210],
		'windmill': ['mechanics', 215],
		'university': ['math', 350],
		'hippodrome': ['horse', 400],
		'media': ['print', 410],
		'bank': ['bank', 420],
		'factory': ['industries', 430],
		'guild': ['knights', 500],
		'lab': ['science', 535],
		'lanterns': ['electricity', 690],
		'hospital': ['medicine', 700],
		'radio_tower': ['radio', 950],
		'pharmacy': ['chemistry', 1010],
		'airport': ['planes', 1045],
		'cosmodrome': ['rockets', 1380],
		'internet_network': ['computers', 2000],
		'atom_lab': ['atom', 2125],
		'reactor': ['atom', 2750],
		'tv_tower': ['telecommunication', 2500],
		'robo_factory': ['robotics', 2850],
		'nuclear_factory': ['nuclear', 5900]
	};
	
	# Common data
	var position : Vector2;
	var level : int;
	var activeUpgrades : Array;
	var population : int;
	var name : String
	
	# Init city
	func _init(_name : String, _pos: Vector2):
		position = _pos;
		level = 1;
		activeUpgrades = [];
		population = 1;
		name = _name;
	
	# Upgrade
	func upgradeCity(name: String, activeTechs: Array):
		if upgradeList[name][0] in activeTechs:
			activeUpgrades.append(name);
			return true;
		else:
			return false;
