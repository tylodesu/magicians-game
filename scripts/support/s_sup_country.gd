extends Node

# Country flag color
enum CountryFlagColor {
	RED,
	ORANGE,
	YELLOW,
	SALAD,
	GREEN,
	CYAN,
	BLUE,
	DEEPBLUE,
	VIOLET,
	PINK,
	CHERRY
}

# Country flag symbol
enum CountryFlagSymbol {
	TRIDENT,
	PHOENIX,
	STAR,
	GEAR,
	LIGHNING,
	WHEAT,
	DRAGON,
	LION,
	CROWN,
	CRESCENT,
	CROSS,
	CROSS_SQUARE,
	EAGLE,
	SUN,
	SWORD,
	STEERINGWHEEL,
	SNEAK
}

# Country flag border
enum CountryFlagBorder {
	NONE,
	WAVES,
	BRICKS,
	CASTLE
}

# Contry resource tyoes
enum CountryResource {
	
	# Common
	FOOD,
	COPPER,
	IRON,
	HORSE,
	URANIUM,
	TITAN,
	
	# Luxury
	GRAPE,
	BANANA,
	PERFUME,
	DIAMONDS
}

# Class describes the country
class Country:
	
	# Common information
	var _name : String;
	var _king : String;
	
	# Flag data
	var _flag_color : CountryFlagColor;
	var _flag_symbol : CountryFlagSymbol;
	var _flag_border : CountryFlagBorder;
	
	# Economy data
	var _eco_balance : int;
	var _eco_tax_percent : int;
	var _eco_trades : Array; # Mark [1]
	
	# Resource data
	var _res_happyness : int; # Range 1-9, def 5
	var _res_list : Array; # Mark [2]
	
	# Research data
	var _tech_tree = SSupTechTree.CountryTechTree.new();

# Marks:
# [1] - 2D Array with item type [String, CountryResouce, int, int] where:
#		[
#			String - country id,
#			CountryReouces - trade resource
#			int - turns left
#			int - profit per turn
#		]
# [2] - 2D Array with item type [CountryResource, bool, int, int] where:
#		[
#			CountryResource - resource type
#			bool - is resource available
#			int - current count
#			int - change per turn
#		]
