extends Node

# Class describes the magic tree
class CountryMagicTree:
	var _tree = {
		'pottery': [1, []], # magic need, dependencies
	};
	
	var currentMagic = [];
	var researchMagic : Array = null; # [int(tech left), String(name)]
	
	# Start research
	func initMagic(name : String):
		if researchMagic != null:
			return false;
		var deps = _tree[name];
		for i in deps[1]:
			if !(i in currentMagic):
				return false;
		researchMagic = [deps[0], name];
		return true;
	
	# Update turns to end of research
	func researchTurns(magLevel : int):
		if researchMagic == null:
			return 0
		else:
			if magLevel > researchMagic[0]:
				currentMagic.append(researchMagic[1]);
				researchMagic = null;
				return 0;
			else:
				var turns = ceil(researchMagic[0] / magLevel);
				researchMagic[0] -= magLevel;
				return turns;
